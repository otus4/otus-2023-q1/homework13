﻿using System.Diagnostics;
using System.Reflection;
using System.Text.Json;

using Reflection.Example.Helpers.Json;
using Reflection.Example.Models;

// Get an initialized instance of class F
var f = new F();
var method = f.GetType().GetMethod("Get", BindingFlags.NonPublic | BindingFlags.Instance);
var instance = method!.Invoke(f, new object[] { }) as F;

// Custom Arange
var count = 1000;
var customSerializer = new CustomSerializer<F>();
var serializedObject = customSerializer.Serialize(instance!);

// Custom serialization
var sw = Stopwatch.StartNew();
for (int i = 0; i < count; i++) customSerializer.Serialize(instance!);
sw.Stop();
Console.WriteLine($"Custom serialization ({count} iterations): {sw.Elapsed.TotalMilliseconds} ms");

// Custom deserialization
sw = Stopwatch.StartNew();
for (int i = 0; i < count; i++) customSerializer.Deserialize(serializedObject);
sw.Stop();
Console.WriteLine($"Custom deserialization ({count} iterations): {sw.Elapsed.TotalMilliseconds} ms");

// System Arange
var stream = new FileStream("f.csv", FileMode.Create, FileAccess.Write);
customSerializer.Set(stream, serializedObject);

stream = new FileStream("f.csv", FileMode.Open, FileAccess.Read);
instance = customSerializer.Get(stream);

serializedObject = JsonSerializer.Serialize(instance);

// System serealization
sw = Stopwatch.StartNew();
for (int i = 0; i < count; i++) JsonSerializer.Serialize(instance!);
sw.Stop();
Console.WriteLine($"System serealization ({count} iterations): {sw.Elapsed.TotalMilliseconds} ms");

// System deserealization
sw = Stopwatch.StartNew();
for (int i = 0; i < count; i++) JsonSerializer.Deserialize<F>(serializedObject);
sw.Stop();
Console.WriteLine($"System deserealization ({count} iterations): {sw.Elapsed.TotalMilliseconds} ms");