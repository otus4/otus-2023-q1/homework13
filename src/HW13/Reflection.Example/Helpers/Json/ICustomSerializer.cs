namespace Reflection.Example.Helpers.Json
{
    public interface ICustomSerializer<T> where T : class, new()
    {
        string Serialize(T t);
        T Deserialize(string s);
    }
}