using System.ComponentModel;
using System.Reflection;

namespace Reflection.Example.Helpers.Json
{
    public class CustomSerializer<T> : ICustomSerializer<T> where T : class, new()
    {
        List<PropertyInfo> _fields { get; set; }

        public CustomSerializer()
        {
            _fields = typeof(T).GetProperties().ToList();
        }

        public T Deserialize(string s)
        {
            var t = new T();

            var input = s.Split(";");
            var names = input[0].Split(",");
            var values = input[1].Split(",");

            names.AsParallel().ForAll(n =>
            {
                var i = Array.IndexOf(names, n);
                var f = _fields.First(x => x.Name == n);
                var c = TypeDescriptor.GetConverter(f.PropertyType);
                var v = c.ConvertFrom(values[i]);
                f.SetValue(t, v);
            });

            return t;
        }

        public string Serialize(T t)
        {
            var names = string.Join(",", _fields.Select(x => x.Name).ToList());
            var values = string.Join(",", _fields.Select(f => ValueToString(f.GetValue(t))).ToList());

            return $"{names};{values}";
        }

        public void Set(Stream stream, string s)
        {
            var sw = new StreamWriter(stream);
            sw.Write(s);
            sw.Close();
        }

        public T Get(Stream stream)
        {
            string s = "";
            using (var sr = new StreamReader(stream))
            {
                s = sr.ReadToEnd();
            }
            return Deserialize(s);
        }

        private string? ValueToString(object? o) => o is null ? string.Empty : o.ToString();
    }
}