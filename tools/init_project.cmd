@echo off
REM Стартовый шаблон инициализации решения
@chcp 65001 >> nul

@REM Задаем имя решения
set SolutionName=HW13
@if exist %SolutionName% goto exitError

REM Инициализируем решение
dotnet new sln -o %SolutionName% -n %SolutionName%
dotnet new gitignore -o %SolutionName%
dotnet new editorconfig -o %SolutionName%

cd %SolutionName%

REM Инициализируем проект сервера
dotnet new console -n Reflection.Example

REM - Добавляем проекты в решение
dotnet sln add .\Reflection.Example\

REM - обновление зависимостей решения
dotnet restore

@echo off
goto exitNormal
:exitError
@echo on
@echo Директория с именем решения уже существует!
@echo off
:exitNormal
@echo Инициализация решения закончена.
exit